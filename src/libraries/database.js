const Sequelize = require('sequelize');

const Database = new Sequelize({
    "use_env_variable": process.env.DATABASE_URL,
    "dialect": "postgres",
    "dialectOptions": {
        "ssl": {
            "require": true,
            "rejectUnauthorized": false
        }
    }
});

// check database connections
async function checkConnection() {
    console.log('trying to connect postgre database...');
    try {
        await Database.authenticate();
        console.log('Database connection has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database ', error);
        throw (error);
    }
}

module.exports = {
    Database,
    checkConnection,
}
