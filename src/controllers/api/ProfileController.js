const jwt = require('jsonwebtoken');
const UserModel = require('../../models/UsersModel');

class ProfileController {
    async getProfile(req, res) {
        try {
            const token = req.query.token;
            console.log('token :: ', token);

            const decode = jwt.verify(token, 'BELAJAR-AUTHENTIKASI');
            console.log('result decode :: ', decode.data);

            const userModel = UserModel();
            const data = await userModel.findOne({
                where: {
                    ID: decode.data.ID,
                }
            });

            res.status(200).json({
                message: 'diproses dari API ProfileController.getProfile',
                data,
            });
        } catch (error) {
            res.status(500).json({
                message: 'failed get profile',
            });
        }
    }
}

module.exports = new ProfileController();
